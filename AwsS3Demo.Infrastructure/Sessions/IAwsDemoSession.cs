﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AwsInfrastructureDemo.Infrastructure.Sessions
{
    public interface IAwsDemoSession
    {
        public int? Id { get; set; }

        public string Username { get; set; }

        public string DisplayName { get; set; }

        public DateTime? LoginAt { get; set; }

        void SaveChanges();

        void Clear();

    }
}
