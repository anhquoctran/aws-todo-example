﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AwsInfrastructureDemo.Infrastructure.Sessions
{
    public class AwsDemoSession : IAwsDemoSession
    {
        public int? Id { get; set; }
        public string Username { get; set; }
        public string DisplayName { get; set; }
        public DateTime? LoginAt { get; set; }

        private readonly ISession _session;
        private const string AWS_DEMO_SESS_KEY = "AWS_SID";

        public AwsDemoSession(ISession session)
        {
            _session = session;
            ReadData();
        }

        public async void Clear()
        {
            _session.Clear();
            await _session.CommitAsync();
        }

        private void ReadData()
        {
            if (_session.TryGetValue(AWS_DEMO_SESS_KEY, out var rawSessData))
            {
                if (rawSessData != null)
                {
                    var jsonData = JsonConvert.DeserializeObject<SessionData>(_session.GetString(AWS_DEMO_SESS_KEY));
                    Id = jsonData.Id;
                    Username = jsonData.Username;
                    DisplayName = jsonData.DisplayName;
                    LoginAt = jsonData.LoginAt;
                }
                
            }
        }

        public async void SaveChanges()
        {
            var sessionData = new SessionData
            {
                Id = Id,
                DisplayName = DisplayName,
                Username = Username,
                LoginAt = LoginAt
            };

            var jsonSess = JsonConvert.SerializeObject(sessionData);
            _session.SetString(AWS_DEMO_SESS_KEY, jsonSess);
            await _session.CommitAsync();
        }

        private class SessionData
        {
            public int? Id { get; set; }
            public string Username { get; set; }
            public string DisplayName { get; set; }
            public DateTime? LoginAt { get; set; }
        }
    }
}
