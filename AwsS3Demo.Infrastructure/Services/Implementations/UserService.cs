﻿using AutoMapper;
using AwsInfrastructureDemo.Core.Enums;
using AwsInfrastructureDemo.Core.Models;
using AwsInfrastructureDemo.Core.Repositories;
using AwsInfrastructureDemo.Core.UnitOfWorks;
using AwsInfrastructureDemo.Infrastructure.DTOs;
using AwsInfrastructureDemo.Infrastructure.Helpers;
using AwsInfrastructureDemo.Infrastructure.Services.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace AwsInfrastructureDemo.Infrastructure.Services.Implementations
{
    public class UserService : BaseRelationalService<User, UserDto>, IUserService
    {
        protected override IRepository<User> Repository => UnitOfWork.UserRepository;

        public UserService(
            IUnitOfWork unitOfWork,
            IMapper mapper,
            ILogger<UserService> logger
            ) : base(unitOfWork, mapper, logger) { }

        public async Task<AuthenticationResultDto> Login(LoginQueryDto loginData)
        {
            var res = new AuthenticationResultDto();

            try
            {
                var username = loginData.Username.Trim();
                var user = await FindAsync(x => x.Username == username);
                if (user != null)
                {
                    user.Password = user.Password.Trim();
                    var checkPassword = SecurityUtils.Check(loginData.Password, user.Password);
                    if (checkPassword)
                    {
                        res.Message = null;
                        res.Name = user.Name;
                        res.UserId = user.Id;
                        res.Username = user.Username;
                        res.Result = OperationResult.Succeed;
                    }
                    else
                    {
                        res.Result = OperationResult.Failed;
                        res.Message = "msg.login_failed";
                    }
                }
                else
                {
                    res.Result = OperationResult.Failed;
                    res.Message = "msg.login_failed";
                }
                return res;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                res.Result = OperationResult.ExceptionFailed;
                return res;
            }
        }

        public OperationResult Authorize(int userId)
        {
            try
            {
                var existsUser = ExistBy(x => x.Id == userId);
                return existsUser ? OperationResult.Succeed : OperationResult.Failed;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return OperationResult.ExceptionFailed;
            }
        }

        public async Task<OperationResult> Add(UserDto user)
        {
            try
            {
                user.Password = SecurityUtils.Hash(user.Password.Trim());
                await CreateAsync(user);

                return OperationResult.Succeed;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return OperationResult.ExceptionFailed;
            }
        }

        public async Task<OperationResult> UpdateUser(UserDto user)
        {
            try
            {
                var existing = await FindAsync(x => x.Id == user.Id);
                if (existing != null)
                {
                    existing.Name = user.Name.Trim();
                    await UpdateAsync(existing);
                    return OperationResult.Succeed;
                }
                return OperationResult.Failed;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return OperationResult.ExceptionFailed;
            }
        }

        public async Task<OperationResult> UpdatePassword(int userId, string oldPassword, string newPassword)
        {
            try
            {
                var existing = await FindAsync(x => x.Id == userId);
                if (existing != null)
                {
                    if (SecurityUtils.Check(oldPassword, existing.Password))
                    {
                        existing.Password = SecurityUtils.Hash(newPassword);
                        await UpdateAsync(existing);
                        return OperationResult.Succeed;
                    }

                    return OperationResult.Failed;
                }
                return OperationResult.ExceptionFailed;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return OperationResult.ExceptionFailed;
            }
        }

        public async Task<OperationResult> CheckUsernameExists(string username, int? currentId = null)
        {
            try
            {
                var query = (from u in AsQueryable() where u.Username == username select u);

                if (currentId > 0)
                {
                    query = query.Where(x => x.Id != currentId);
                }

                return !(await query.AnyAsync()) ? OperationResult.Succeed : OperationResult.Failed;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return OperationResult.ExceptionFailed;
            }
        }

        public async Task<KeyValuePair<OperationResult, UserDto>> GetUserById(int id)
        {
            try
            {
                var user = await FindAsync(x => x.Id == id);
                if (user != null)
                {
                    return new KeyValuePair<OperationResult, UserDto>(OperationResult.Succeed, user);
                }

                return new KeyValuePair<OperationResult, UserDto>(OperationResult.Failed, null);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return new KeyValuePair<OperationResult, UserDto>(OperationResult.ExceptionFailed, null);
            }
        }
    }
}