﻿using AutoMapper;
using AwsInfrastructureDemo.Core.Abstractions;
using AwsInfrastructureDemo.Core.Repositories;
using AwsInfrastructureDemo.Core.UnitOfWorks;
using AwsInfrastructureDemo.Infrastructure.Services.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace AwsInfrastructureDemo.Infrastructure.Services.Implementations
{
    public abstract class BaseRelationalService<TEntity, TDto> : IRelationalService<TEntity, TDto>
        where TEntity : class, IEntity
        where TDto : class, new()
    {
        protected readonly IUnitOfWork UnitOfWork;
        protected readonly IMapper Mapper;
        protected readonly ILogger Logger;

        protected abstract IRepository<TEntity> Repository { get; }

        protected BaseRelationalService(IUnitOfWork unitOfWork, IMapper mapper, ILogger logger)
        {
            UnitOfWork = unitOfWork;
            Mapper = mapper;
            Logger = logger;
        }

        public virtual TDto Create(TDto dto)
        {
            var entity = Repository.Add(DtoToEntity(dto));
            UnitOfWork.Save();
            return EntityToDto(entity);
        }

        public virtual async Task<TDto> CreateAsync(TDto dto, CancellationToken cts = default(CancellationToken))
        {
            var entity = Repository.Add(DtoToEntity(dto));
            await UnitOfWork.SaveAsync(cts);
            return EntityToDto(entity);
        }

        public virtual TDto Update(TDto dto)
        {
            var entity = Repository.Update(DtoToEntity(dto));
            UnitOfWork.Save();
            return EntityToDto(entity);
        }

        public virtual async Task<TDto> UpdateAsync(TDto dto, CancellationToken cts = default(CancellationToken))
        {
            var entity = Repository.Update(DtoToEntity(dto));
            await UnitOfWork.SaveAsync(cts);
            return EntityToDto(entity);
        }

        public virtual void UpdateRange(IEnumerable<TDto> dtos)
        {
            Repository.UpdateRange(DtoToEntity(dtos));
            UnitOfWork.Save();
        }

        public virtual int UpdateBy(Expression<Func<TEntity, bool>> condition, Expression<Func<TEntity, TEntity>> updateFactory)
        {
            return Repository.UpdateBy(condition, updateFactory);
        }

        public virtual Task<int> UpdateByAsync(Expression<Func<TEntity, bool>> condition, Expression<Func<TEntity, TEntity>> updateFactory, CancellationToken cts = default(CancellationToken))
        {
            return Repository.UpdateByAsync(condition, updateFactory, cts);
        }

        public virtual void Delete(IEnumerable<TDto> list)
        {
            var listEntity = DtoToEntity(list);
            Repository.Delete(listEntity);
            UnitOfWork.Save();
        }

        public virtual int DeleteBy(Expression<Func<TEntity, bool>> predicate)
        {
            return Repository.DeleteBy(predicate);
        }

        public virtual Task<int> DeleteByAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cts = default(CancellationToken))
        {
            return Repository.DeleteByAsync(predicate, cts);
        }

        public virtual Task DeleteAsync(TDto dto, CancellationToken cts = default(CancellationToken))
        {
            var entity = DtoToEntity(dto);
            return Repository.DeleteAsync(entity, cts);
        }

        public virtual Task<int> DeleteAsync(IEnumerable<TDto> dtos, CancellationToken cts = default(CancellationToken))
        {
            var entities = DtoToEntity(dtos);
            return Repository.DeleteAsync(entities, cts);
        }

        public virtual async Task<ICollection<TDto>> FindAllAsync(Expression<Func<TEntity, bool>> predicate = null, CancellationToken cts = default(CancellationToken))
        {
            return EntityToDto(await Repository.GetAllAsync(predicate, cts)).ToList();
        }

        public virtual TDto Find(Expression<Func<TEntity, bool>> condition)
        {
            return EntityToDto(Repository.Find(condition));
        }

        public virtual async Task<TDto> FindAsync(Expression<Func<TEntity, bool>> condition, bool noTracking = false, CancellationToken cts = default(CancellationToken))
        {
            return EntityToDto(await Repository.FindAsync(condition, noTracking, cts));
        }

        public virtual bool ExistBy(Expression<Func<TEntity, bool>> predicate)
        {
            return Repository.ExistsBy(predicate);
        }

        public Task<bool> ExistByAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cts = default(CancellationToken))
        {
            return Repository.ExistsByAsync(predicate, cts);
        }

        protected TDto EntityToDto(TEntity entity)
        {
            return Mapper.Map<TDto>(entity);
        }

        protected TEntity DtoToEntity(TDto dto)
        {
            return Mapper.Map<TEntity>(dto);
        }

        protected IEnumerable<TDto> EntityToDto(IEnumerable<TEntity> entities)
        {
            return Mapper.Map<IEnumerable<TDto>>(entities);
        }

        protected IEnumerable<TEntity> DtoToEntity(IEnumerable<TDto> dto)
        {
            return Mapper.Map<IEnumerable<TEntity>>(dto);
        }

        public virtual ICollection<TDto> FindAll(Expression<Func<TEntity, bool>> predicate = null)
        {
            return EntityToDto(Repository.GetAll(predicate)).ToList();
        }

        public virtual IQueryable<TEntity> AsQueryable(Expression<Func<TEntity, bool>> predicate = null)
        {
            return Repository.AsQueryable(predicate);
        }

        public virtual int Count(Expression<Func<TEntity, bool>> predicate = null)
        {
            return Repository.Count(predicate);
        }

        public virtual Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate = null, CancellationToken cts = default(CancellationToken))
        {
            return Repository.CountAsync(predicate, cts);
        }

        public virtual Task<int> MaxByAsync(Expression<Func<TEntity, int>> predicate, CancellationToken cts = default(CancellationToken))
        {
            return Repository.MaxByAsync(predicate, cts);
        }

        public virtual int MaxBy(Expression<Func<TEntity, int>> predicate)
        {
            return Repository.MaxBy(predicate);
        }
    }
}