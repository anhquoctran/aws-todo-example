﻿using AutoMapper;
using AwsInfrastructureDemo.Core.Enums;
using AwsInfrastructureDemo.Core.Models;
using AwsInfrastructureDemo.Core.Repositories;
using AwsInfrastructureDemo.Core.UnitOfWorks;
using AwsInfrastructureDemo.Infrastructure.DTOs;
using AwsInfrastructureDemo.Infrastructure.Helpers;
using AwsInfrastructureDemo.Infrastructure.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AwsInfrastructureDemo.Infrastructure.Services.Implementations
{
    public class TodoService : BaseRelationalService<Todo, TodoDto>, ITodoService
    {
        protected override IRepository<Todo> Repository => UnitOfWork.TodoRepository;

        public TodoService(
            IUnitOfWork unitOfWork,
            IMapper mapper,
            ILogger<TodoService> logger
            ) : base(unitOfWork, mapper, logger) { }

        public async Task<KeyValuePair<OperationResult, TodoDto>> GetTodo(int id)
        {
            try
            {
                var todo = await FindAsync(x => x.Id == id);
                return new KeyValuePair<OperationResult, TodoDto>(todo != null ? OperationResult.Succeed : OperationResult.Failed, todo);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return new KeyValuePair<OperationResult, TodoDto>(OperationResult.ExceptionFailed, null);
            }
        }

        public async Task<PaginatedList<TodoDto>> GetListTodoPagination(string keyword, int page = 1, int pageSize = 20)
        {
            try
            {
                var query = AsQueryable();

                if (!StringUtils.CheckIsEmptyOrWhitespaceString(keyword))
                {
                    keyword = keyword.Trim().ToLower();
                    query = query.Where(x => EF.Functions.Like(x.Name.Trim().ToLower(), $"%{keyword}%"));
                }

                var data = query.Select(x => new TodoDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    TodoItems = x.TodoItems
                        .Select(y => new TodoItemDto
                        {
                            Id = y.Id,
                            IsDone = y.IsDone,
                            CreatedAt = y.CreatedAt,
                            TodoContent = y.TodoContent,
                            UpdatedAt = y.UpdatedAt,
                            TodoId = y.TodoId
                        })
                        .ToArray(),
                    CreatedAt = x.CreatedAt,
                    UpdatedAt = x.UpdatedAt
                });

                return await PaginatedList<TodoDto>.CreateAsync(data, null, page, pageSize);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<ICollection<TodoDto>> GetListTodo()
        {
            try
            {
                return EntityToDto(await AsQueryable().ToArrayAsync()).ToArray();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<OperationResult> CreateTodo(TodoDto todo)
        {
            try
            {
                await CreateAsync(todo);
                return OperationResult.Succeed;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return OperationResult.ExceptionFailed;
            }
        }

        public async Task<OperationResult> UpdateTodo(TodoDto todo)
        {
            try
            {
                var exists = await FindAsync(x => x.Id == todo.Id);
                if (exists != null)
                {
                    exists.Name = todo.Name.Trim();
                    await UpdateAsync(exists);
                    return OperationResult.Succeed;
                }
                else
                {
                    return OperationResult.Failed;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return OperationResult.ExceptionFailed;
            }
        }

        public async Task<OperationResult> DeleteTodo(int id)
        {
            try
            {
                var deleted = await DeleteByAsync(x => x.Id == id);
                return deleted > 0 ? OperationResult.Succeed : OperationResult.Failed;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return OperationResult.ExceptionFailed;
            }
        }

        public async Task<OperationResult> CheckNameExists(string name)
        {
            try
            {
                name = name.Trim().ToLower();
                return !await ExistByAsync(x => x.Name.Trim().ToLower() == name) ? OperationResult.Succeed : OperationResult.Failed;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                return OperationResult.ExceptionFailed;
            }
        }
    }
}