﻿using AwsInfrastructureDemo.Infrastructure.Services.Implementations;
using AwsInfrastructureDemo.Infrastructure.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace AwsInfrastructureDemo.Infrastructure.Services
{
    public static class ServiceDependenciesInjectionExtensions
    {
        public static IServiceCollection RegisterAllServices(this IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITodoService, TodoService>();

            return services;
        }
    }
}