﻿using AwsInfrastructureDemo.Core.Enums;
using AwsInfrastructureDemo.Infrastructure.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AwsInfrastructureDemo.Infrastructure.Services.Interfaces
{
    public interface IUserService
    {
        Task<AuthenticationResultDto> Login(LoginQueryDto loginData);

        OperationResult Authorize(int userId);

        Task<OperationResult> Add(UserDto user);

        Task<OperationResult> UpdateUser(UserDto user);

        Task<OperationResult> UpdatePassword(int userId, string oldPassword, string newPassword);

        Task<OperationResult> CheckUsernameExists(string username, int? currentId = null);

        Task<KeyValuePair<OperationResult, UserDto>> GetUserById(int id);
    }
}