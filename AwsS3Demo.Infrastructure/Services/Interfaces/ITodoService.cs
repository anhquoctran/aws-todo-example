﻿using AwsInfrastructureDemo.Core.Enums;
using AwsInfrastructureDemo.Infrastructure.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AwsInfrastructureDemo.Infrastructure.Services.Interfaces
{
    public interface ITodoService
    {
        Task<PaginatedList<TodoDto>> GetListTodoPagination(string name, int page = 1, int pageSize = 20);

        Task<KeyValuePair<OperationResult, TodoDto>> GetTodo(int id);

        Task<ICollection<TodoDto>> GetListTodo();

        Task<OperationResult> CreateTodo(TodoDto todo);

        Task<OperationResult> UpdateTodo(TodoDto todo);

        Task<OperationResult> DeleteTodo(int id);

        Task<OperationResult> CheckNameExists(string name);
    }
}