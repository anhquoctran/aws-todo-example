﻿using AwsInfrastructureDemo.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace AwsInfrastructureDemo.Infrastructure.Services.Interfaces
{
    public interface IRelationalService<TEntity, TDto>
        : IBaseService
        where TEntity : class, IEntity
        where TDto : class, new()

    {
        /// <summary>
        /// Create and save resource synchronously
        /// </summary>
        /// <param name="dto">DTO object containing new data to create</param>
        /// <returns></returns>
        TDto Create(TDto dto);

        /// <summary>
        /// Create resource asynchronously
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="cts"></param>
        /// <returns></returns>
        Task<TDto> CreateAsync(TDto dto, CancellationToken cts = default(CancellationToken));

        /// <summary>
        /// Update resource aynchronously
        /// </summary>
        /// <param name="dto">DTO object containing new data to update</param>
        /// <returns></returns>
        TDto Update(TDto dto);

        /// <summary>
        /// Update resource asynchronously
        /// </summary>
        /// <param name="tdo"></param>
        /// <param name="cts"></param>
        /// <returns></returns>
        Task<TDto> UpdateAsync(TDto tdo, CancellationToken cts = default(CancellationToken));

        void UpdateRange(IEnumerable<TDto> dtos);

        int UpdateBy(Expression<Func<TEntity, bool>> condition, Expression<Func<TEntity, TEntity>> updateFactory);

        Task<int> UpdateByAsync(Expression<Func<TEntity, bool>> condition,
            Expression<Func<TEntity, TEntity>> updateFactory, CancellationToken cts = default(CancellationToken));

        /// <summary>
        /// Delete multiple entities by list
        /// </summary>
        /// <param name="listId">List of entity's ID</param>
        void Delete(IEnumerable<TDto> listId);

        int DeleteBy(Expression<Func<TEntity, bool>> predicate);

        Task<int> DeleteByAsync(Expression<Func<TEntity, bool>> predicate,
            CancellationToken cts = default(CancellationToken));

        /// <summary>
        ///
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="cts"></param>
        /// <returns></returns>
        Task DeleteAsync(TDto dto, CancellationToken cts = default(CancellationToken));

        /// <summary>
        ///
        /// </summary>
        /// <param name="dtos"></param>
        /// <param name="cts"></param>
        /// <returns></returns>
        Task<int> DeleteAsync(IEnumerable<TDto> dtos, CancellationToken cts = default(CancellationToken));

        /// <summary>
        /// Find all entities by predicate Condition
        /// </summary>
        /// <param name="predicate">Condition</param>
        /// <returns></returns>
        ICollection<TDto> FindAll(Expression<Func<TEntity, bool>> predicate = null);

        /// <summary>
        /// Find all async
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="cts"></param>
        /// <returns></returns>
        Task<ICollection<TDto>> FindAllAsync(
            Expression<Func<TEntity, bool>> predicate = null,
            CancellationToken cts = default(CancellationToken)
            );

        /// <summary>
        /// Find entity by predicate Condition
        /// </summary>
        /// <param name="condition">Condition</param>
        /// <returns></returns>
        TDto Find(Expression<Func<TEntity, bool>> condition);

        Task<TDto> FindAsync(Expression<Func<TEntity, bool>> condition, bool noTracking = false,
            CancellationToken cts = default(CancellationToken));

        /// <summary>
        /// Check entity exists by property
        /// </summary>
        /// <param name="predicate">Condition</param>
        /// <returns></returns>
        bool ExistBy(Expression<Func<TEntity, bool>> predicate);

        Task<bool> ExistByAsync(Expression<Func<TEntity, bool>> predicate,
            CancellationToken cts = default(CancellationToken));

        /// <summary>
        /// Query all date return IQueryable
        /// </summary>
        /// <param name="predicate">Condition</param>
        /// <returns></returns>
        IQueryable<TEntity> AsQueryable(Expression<Func<TEntity, bool>> predicate = null);

        int Count(Expression<Func<TEntity, bool>> predicate = null);

        Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate = null, CancellationToken cts = default(CancellationToken));

        Task<int> MaxByAsync(Expression<Func<TEntity, int>> predicate, CancellationToken cts = default(CancellationToken));

        int MaxBy(Expression<Func<TEntity, int>> predicate);
    }
}