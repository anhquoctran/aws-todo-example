﻿using System;

namespace AwsInfrastructureDemo.Infrastructure.DTOs
{
    public class TodoItemDto
    {
        public int Id { get; set; }
        public int TodoId { get; set; }
        public bool IsDone { get; set; }
        public string TodoContent { get; set; }
        public string TodoImageUrl { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public virtual TodoDto Todo { get; set; }
    }
}