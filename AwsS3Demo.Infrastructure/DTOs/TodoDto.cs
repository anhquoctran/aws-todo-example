﻿using System;
using System.Collections.Generic;

namespace AwsInfrastructureDemo.Infrastructure.DTOs
{
    public class TodoDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public virtual ICollection<TodoItemDto> TodoItems { get; set; }
    }
}