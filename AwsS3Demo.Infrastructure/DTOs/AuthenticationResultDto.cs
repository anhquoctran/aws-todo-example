﻿using AwsInfrastructureDemo.Core.Enums;

namespace AwsInfrastructureDemo.Infrastructure.DTOs
{
    public class AuthenticationResultDto
    {
        public OperationResult Result { get; set; }

        public string Message { get; set; }

        public int UserId { get; set; }

        public string Username { get; set; }

        public string Name { get; set; }
    }
}