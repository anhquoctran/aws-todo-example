﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AwsInfrastructureDemo.Infrastructure.DTOs
{
    public class PaginatedList<T> : List<T> where T : new()
    {
        public int PageIndex { get; }
        public int TotalPages { get; }
        public int TotalItems { get; }
        public int ShowFrom { get; }
        public int ShowTo { get; }
        public int ItemsPerPage { get; }

        public PaginatedList(IEnumerable<T> items, int totalItems, int pageIndex, int pageSize)
        {
            ItemsPerPage = pageSize;
            PageIndex = pageIndex;
            TotalPages = (int)Math.Ceiling(totalItems / (double)pageSize);
            TotalItems = totalItems;
            ShowFrom = ((pageIndex - 1) * pageSize) + 1;
            ShowTo = pageSize * pageIndex > totalItems ? totalItems : pageSize * pageIndex;
            AddRange(items);
        }

        public PaginatedList(
            IEnumerable<T> items,
            int totalItems,
            int pageIndex,
            int showFrom,
            int showTo,
            int pageSize
            )
        {
            AddRange(items);
            PageIndex = pageIndex;
            TotalItems = totalItems;
            ShowFrom = showFrom;
            ShowTo = showTo;
            TotalPages = (int)Math.Ceiling(totalItems / (double)pageSize);
        }

        public bool HasPreviousPage => PageIndex > 1;

        public bool HasNextPage => PageIndex < TotalPages;

        public static async Task<PaginatedList<T>> CreateAsync(IQueryable<T> source, int? preCount, int? pageIndex, int pageSize = 20)
        {
            int count;
            if (!preCount.HasValue || preCount <= 0)
            {
                count = await source.CountAsync();
            }
            else
            {
                count = preCount.Value;
            }
            var page = (pageIndex > 0 ? pageIndex : 1).GetValueOrDefault();
            var offset = (page - 1) * pageSize;
            var entities = await source.Skip(offset)
                .Take(pageSize)
                .ToArrayAsync();

            return new PaginatedList<T>(entities, count, page, pageSize);
        }

        public static PaginatedList<T> Create(IQueryable<T> source, int? pageIndex, int? pageSize = 20)
        {
            var count = source.Count();
            var page = pageIndex ?? 1;
            var perPage = pageSize ?? 20;
            var entities = source.Skip((page - 1) * perPage)
                .Take(perPage)
                .ToArray();

            return new PaginatedList<T>(entities, count, page, perPage);
        }

        public static PaginatedList<T> AsEmpty()
        {
            return new PaginatedList<T>(new T[0], 0, 1, 20);
        }
    }
}
