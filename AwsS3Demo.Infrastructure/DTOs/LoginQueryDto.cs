﻿namespace AwsInfrastructureDemo.Infrastructure.DTOs
{
    public class LoginQueryDto
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}