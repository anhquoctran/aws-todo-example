﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AwsInfrastructureDemo.Infrastructure.Helpers
{
    public static class ModelStateExtensions
    {
        public static string[] GetErrorMessages(this ModelStateDictionary.ValueEnumerable values)
        {
            return values
                .SelectMany(x => x.Errors)
                .Select(x => x.ErrorMessage)
                .Distinct()
                .ToArray();
        }

        public static IDictionary<string, string[]> GetMessagesWithKey(this ModelStateDictionary modelState)
        {
            var dict = new Dictionary<string, string[]>();
            var keys = modelState.Keys.ToArray();
            foreach (var key in keys)
            {
                var entry = modelState[key];
                if (entry.Errors.Count > 0)
                {
                    if (!StringUtils.CheckIsEmptyOrWhitespaceString(key))
                    {
                        var errors = entry.Errors
                            .Where(x => !StringUtils.CheckIsEmptyOrWhitespaceString(x.ErrorMessage))
                            .Select(x => x.ErrorMessage);

                        if (errors.Any())
                        {
                            dict.Add(key, entry.Errors.Select(x => x.ErrorMessage).ToArray());
                        }
                    }
                }
            }
            return dict;
        }

        public static void SetModelStateError(this ModelStateDictionary modelState, IDictionary<string, string[]> dictError)
        {
            if (dictError == null) return;
            foreach (var (field, messages) in dictError)
            {
                foreach (var message in messages)
                {
                    modelState.AddModelError(field, message);
                }
            }
        }

        public static void SetModelStateError(this ModelStateDictionary modelState, IDictionary<string, string> dictError)
        {
            if (dictError == null) return;
            foreach (var (field, message) in dictError)
            {
                modelState.AddModelError(field, message);
            }
        }
    }
}
