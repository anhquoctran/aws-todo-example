﻿using static BCrypt.Net.BCrypt;

namespace AwsInfrastructureDemo.Infrastructure.Helpers
{
    public static class SecurityUtils
    {
        public static string Hash(string raw, int round = 10)
        {
            if (!StringUtils.CheckIsEmptyOrWhitespaceString(raw))
            {
                var hash = HashPassword(raw, round);
                return hash;
            }
            return null;
        }

        public static bool Check(string raw, string cipher)
        {
            if (!StringUtils.CheckIsEmptyOrWhitespaceString(raw) && !StringUtils.CheckIsEmptyOrWhitespaceString(cipher))
            {
                return Verify(raw, cipher);
            }

            return false;
        }
    }
}