﻿using AwsInfrastructureDemo.Infrastructure.Constants;
using AwsInfrastructureDemo.Infrastructure.Sessions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace AwsInfrastructureDemo.Infrastructure.Helpers
{
    public static class RequestExtensions
    {
        public static void RegisterAwsDemoSession(this HttpContext httpContext, IAwsDemoSession koenkyokaiSession)
        {
            httpContext.Items[HttpConstants.AWS_SESS_NAME] = koenkyokaiSession;
        }

        public static IAwsDemoSession GetAwsDemoSession(this HttpContext httpContext)
        {
            if (httpContext.Items.TryGetValue(HttpConstants.AWS_SESS_NAME, out var session))
            {
                return (IAwsDemoSession)session;
            }

            return new AwsDemoSession(httpContext.Session);
        }
    }
}
