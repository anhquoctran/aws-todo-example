﻿using Newtonsoft.Json.Linq;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text.RegularExpressions;

namespace AwsInfrastructureDemo.Infrastructure.Helpers
{
    public static class StringUtils
    {
        public static bool CheckIsEmptyOrWhitespaceString(string query)
        {
            if (query == null) return true;
            return string.IsNullOrEmpty(query) || string.IsNullOrWhiteSpace(query);
        }

        public static string RemoveAllSpaceChars(this string str)
        {
            return str.Replace(" ", string.Empty, StringComparison.InvariantCultureIgnoreCase);
        }

        public static string GetGuid()
        {
            return Guid.NewGuid().ToString("N").ToLowerInvariant();
        }

        public static bool IsValidGuid(string guidText)
        {
            var parsedOk = Guid.TryParse(guidText, out var _);
            return parsedOk;
        }

        public static bool IsValidGuid(string guidText, string formatTry)
        {
            var parsedOk = Guid.TryParseExact(guidText, formatTry, out var _);
            return parsedOk;
        }

        public static bool IsValidJson(string rawText)
        {
            if (CheckIsEmptyOrWhitespaceString(rawText)) return false;
            try
            {
                JToken.Parse(rawText);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Get the Description from the DescriptionAttribute.
        /// </summary>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static string GetDescription(this Enum enumValue)
        {
            return enumValue.GetType()
                       .GetMember(enumValue.ToString())
                       .First()
                       .GetCustomAttribute<DescriptionAttribute>()?
                       .Description ?? string.Empty;
        }

        public static bool Compare(string s1, string s2, bool ignoreCase = false)
        {
            return string.Equals(s1, s2, ignoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.InvariantCulture);
        }

        public static bool IsValidUrl(string urlLike)
        {
            if (CheckIsEmptyOrWhitespaceString(urlLike)) return false;

            return Uri.TryCreate(urlLike, UriKind.RelativeOrAbsolute, out var uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
        }

        private static string DomainMapper(Match match)
        {
            var idn = new IdnMapping();

            // Pull out and process domain name (throws ArgumentException on invalid)
            var domainName = idn.GetAscii(match.Groups[2].Value);

            return match.Groups[1].Value + domainName;
        }

        public static bool IsValidEmail(string email)
        {
            if (CheckIsEmptyOrWhitespaceString(email)) return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper, RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
            catch (ArgumentException)
            {
                return false;
            }

            try
            {
                _ = new MailAddress(email);
                var indexOfAtSign = email.IndexOf('@');
                var validMethod1 =
                  indexOfAtSign > 0 &&
                  indexOfAtSign != email.Length - 1 &&
                  indexOfAtSign == email.LastIndexOf('@')
                ;
                var validMethod2 = Regex.IsMatch(email,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));

                return validMethod1 && validMethod2;
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        /// <summary>
        /// Parse email address and split two parts: user and host
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static (string, string) ParseEmailAddress(string email)
        {
            var emailParsed = new MailAddress(email);
            return (emailParsed.User, emailParsed.Host);
        }

        public static string RejectMarks(string text)
        {
            if (string.IsNullOrEmpty(text)) return string.Empty;
            var pattern = new string[7];
            var replaceChar = new char[14];

            replaceChar[0] = 'a';
            replaceChar[1] = 'd';
            replaceChar[2] = 'e';
            replaceChar[3] = 'i';
            replaceChar[4] = 'o';
            replaceChar[5] = 'u';
            replaceChar[6] = 'y';
            replaceChar[7] = 'A';
            replaceChar[8] = 'D';
            replaceChar[9] = 'E';
            replaceChar[10] = 'I';
            replaceChar[11] = 'O';
            replaceChar[12] = 'U';
            replaceChar[13] = 'Y';

            pattern[0] = "(á|à|ả|ã|ạ|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ)";
            pattern[1] = "đ";
            pattern[2] = "(é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ)";
            pattern[3] = "(í|ì|ỉ|ĩ|ị)";
            pattern[4] = "(ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ)";
            pattern[5] = "(ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự)";
            pattern[6] = "(ý|ỳ|ỷ|ỹ|ỵ)";

            for (var i = 0; i < pattern.Length; i++)
            {
                var matchs = Regex.Matches(text, pattern[i], RegexOptions.IgnoreCase);
                foreach (Match m in matchs)
                {
                    var ch = char.IsLower(m.Value[0]) ? replaceChar[i] : replaceChar[i + 7];
                    text = text.Replace(m.Value[0], ch);
                }
            }
            return text;
        }

        public static string GenerateRandomString(byte length = 8)
        {
            const string charaters = @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var result = string.Empty;
            var characterLength = charaters.Length;
            var rand = new Random();
            for (var i = 0; i < length; i++)
            {
                result += charaters[rand.Next(0, characterLength)];
            }
            return result;
        }

        /// <summary>
        /// Generate unique id
        /// </summary>
        /// <returns></returns>
        public static string GenerateId()
        {
            var i = Guid.NewGuid()
                .ToByteArray()
                .Aggregate<byte, long>(1, (current, b) => current * (b + 1));
            return $"{i - DateTime.UtcNow.Ticks:x}";
        }

        public static int? ToNullableInt(this string str)
        {
            if (int.TryParse(str, out var v)) return v;

            return null;
        }

        public static float? ToNullableFloat(this string str)
        {
            if (float.TryParse(str, out var v)) return v;

            return null;
        }

        public static string ConvertToSnakeCase(string str)
        {
            return CheckIsEmptyOrWhitespaceString(str) ? string.Empty : str.ToLowerInvariant().Replace('-', '_');
        }

        public static string FormatSizeInBytes(long fileLength)
        {
            var sizes = new string[] { "B", "KB", "MB", "GB", "TB" };
            var order = 0;
            while (fileLength >= 1024 && order < sizes.Length - 1)
            {
                order++;
                fileLength /= 1024;
            }

            // Adjust the format string to your preferences. For example "{0:0.#}{1}" would
            // show a single decimal place, and no space.
            return string.Format("{0:0.#} {1}", fileLength, sizes[order]);
        }

        public static DateTime ConvertStringToDateTime(int year, int month, int day)
        {
            if (year == 0 || month == 0 || day == 0)
            {
                return DateTime.Today;
            }
            //var tmpDate = year.ToString() + "-" + month.ToString() + "-" + day.ToString();
            //var date = Convert.ToDateTime(tmpDate);
            var date = new DateTime(year, month, day);
            if (date == DateTime.MinValue || date == DateTime.MaxValue)
            {
                return DateTime.Today;
            }
            return date;
        }
    }
}