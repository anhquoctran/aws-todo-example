﻿using AutoMapper;
using AwsInfrastructureDemo.Core.Models;
using AwsInfrastructureDemo.Infrastructure.DTOs;

namespace AwsInfrastructureDemo.Infrastructure.Mappings.Profiles
{
    public class TodoItemProfile : Profile
    {
        public TodoItemProfile()
        {
            CreateMap<TodoItem, TodoItemDto>();

            CreateMap<TodoItemDto, TodoItem>();
        }
    }
}