﻿using AutoMapper;
using AwsInfrastructureDemo.Core.Models;
using AwsInfrastructureDemo.Infrastructure.DTOs;

namespace AwsInfrastructureDemo.Infrastructure.Mappings.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDto>();

            CreateMap<UserDto, User>();
        }
    }
}