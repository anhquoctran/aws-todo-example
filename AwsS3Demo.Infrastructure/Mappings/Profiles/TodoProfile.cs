﻿using AutoMapper;
using AwsInfrastructureDemo.Core.Models;
using AwsInfrastructureDemo.Infrastructure.DTOs;

namespace AwsInfrastructureDemo.Infrastructure.Mappings.Profiles
{
    public class TodoProfile : Profile
    {
        public TodoProfile()
        {
            CreateMap<Todo, TodoDto>();

            CreateMap<TodoDto, Todo>();
        }
    }
}