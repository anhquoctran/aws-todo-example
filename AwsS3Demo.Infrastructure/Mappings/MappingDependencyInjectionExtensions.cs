﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AwsInfrastructureDemo.Infrastructure.Mappings
{
    public static class MappingDependencyInjectionExtensions
    {
        private static ICollection<Profile> RetrieveProfiles()
        {
            var currentDomain = AppDomain.CurrentDomain;
            var asms = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(x => x.Namespace == "AwsInfrastructureDemo.Infrastructure.Mappings.Profiles" && x.IsSubclassOf(typeof(Profile)))
                .ToArray();

            return asms.Select(x =>
            {
                var profile = (Profile)Activator.CreateInstance(x);
                return profile;
            }).ToArray();
        }

        public static IServiceCollection AddAwsMapping(this IServiceCollection services)
        {
            var profiles = RetrieveProfiles();

            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfiles(profiles);
            });

            var mapper = mapperConfig.CreateMapper();

            services.AddSingleton(mapper);

            return services;
        }
    }
}