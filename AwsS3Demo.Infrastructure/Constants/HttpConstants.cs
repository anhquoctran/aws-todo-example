﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AwsInfrastructureDemo.Infrastructure.Constants
{
    public static class HttpConstants
    {
        public const string AWS_SESS_NAME = "AWS_DEMO";
        public const string STATIC_ROUTE_PREFIX = "/static";
    }
}
