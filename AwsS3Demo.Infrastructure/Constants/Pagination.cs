﻿namespace AwsInfrastructureDemo.Infrastructure.Constants
{
    public static class Pagination
    {
        public const int DEFAULT_PAGE = 1;

        public const int DEFAULT_PAGE_SIZE = 20;
    }
}