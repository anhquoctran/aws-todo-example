﻿using AwsInfrastructureDemo.Infrastructure.Helpers;
using FluentValidation.Resources;
using FluentValidation.Validators;
using Microsoft.AspNetCore.Http;

namespace AwsInfrastructureDemo.Validations.CustomRules
{
    public class HasFileLengthValidator : PropertyValidator
    {
        private const long DEFAULT_LENGTH = 2 * 1024 * 1024; // Default size is 2MB, size must be less than or equals default length

        private readonly long _length;

        public HasFileLengthValidator(long fileLength = DEFAULT_LENGTH)
            : base(new LanguageStringSource(nameof(HasFileLengthValidator)))
        {
            if (fileLength <= 0) fileLength = DEFAULT_LENGTH;
            _length = fileLength;
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (context.PropertyValue == null) return true;
            var file = (IFormFile)context.PropertyValue;
            context.MessageFormatter.AppendArgument("AllowedLength", StringUtils.FormatSizeInBytes(_length));
            return file.Length <= _length;
        }
    }
}