﻿using System;
using System.Net.Mail;
using System.Text.RegularExpressions;
using AwsInfrastructureDemo.Infrastructure.Helpers;
using FluentValidation.Resources;
using FluentValidation.Validators;

namespace AwsInfrastructureDemo.Validations.CustomRules
{
    public class IsEmailValidator : PropertyValidator
    {
        public IsEmailValidator() : base(new LanguageStringSource(nameof(IsEmailValidator))) {}

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var propValue = context.PropertyValue;

            if (propValue == null) return true;

            var mailAddressRawValue = propValue.ToString();

            const string regexPattern = @"^([\w\.\-]+)@([\w_\-]+)\.([\w_\.\-]*)[a-z][a-z]$";

            return IsValidEmail(mailAddressRawValue) && Regex.IsMatch(mailAddressRawValue, regexPattern, RegexOptions.IgnoreCase | RegexOptions.Multiline);
        }

        private static bool IsValidEmail(string value)
        {
            try
            {
                return StringUtils.IsValidEmail(value);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}