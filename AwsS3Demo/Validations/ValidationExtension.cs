﻿using AwsInfrastructureDemo.Validations.CustomRules;
using FluentValidation;
using Microsoft.AspNetCore.Http;

namespace AwsInfrastructureDemo.Validations
{
    /// <summary>
    /// Create Fluent validation extension methods here
    /// </summary>
    public static class ValidationExtension
    {
        /// <summary>
        /// Validate string value is digits or not
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ruleBuilder"></param>
        /// <returns></returns>
        public static IRuleBuilderOptions<T, string> IsDigits<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new IsDigitsValidator());
        }

        /// <summary>
        /// Validate file has allowed extensions
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ruleBuilder"></param>
        /// <param name="extensions">list extensions allowed</param>
        /// <returns></returns>
        public static IRuleBuilderOptions<T, IFormFile> HasFileExtension<T>(
            this IRuleBuilder<T, IFormFile> ruleBuilder,
            params string[] extensions)
        {
            return ruleBuilder.SetValidator(new HasFileExtensionValidator(extensions));
        }

        /// <summary>
        /// Validate file has mime types
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ruleBuilder"></param>
        /// <param name="mimeTypes">List mime types allowed</param>
        /// <returns></returns>
        public static IRuleBuilderOptions<T, IFormFile> HasMimeType<T>(this IRuleBuilder<T, IFormFile> ruleBuilder, params string[] mimeTypes)
        {
            return ruleBuilder.SetValidator(new HasMimeTypeValidator(mimeTypes));
        }

        /// <summary>
        /// Validate file must be less than or equals max length (in byte)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ruleBuilder"></param>
        /// <param name="maxLength">Maximum length allowed</param>
        /// <returns></returns>
        public static IRuleBuilderOptions<T, IFormFile> HasFileLength<T>(this IRuleBuilder<T, IFormFile> ruleBuilder, long maxLength)
        {
            return ruleBuilder.SetValidator(new HasFileLengthValidator(maxLength));
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ruleBuilder"></param>
        /// <param name="minLength"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static IRuleBuilderOptions<T, string> PasswordLength<T>(
            this IRuleBuilder<T, string> ruleBuilder,
            int minLength,
            int maxLength
            )
        {
            return ruleBuilder.SetValidator(new PasswordLengthValidator(minLength, maxLength));
        }

        public static IRuleBuilderOptions<T, string> IsEmail<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new IsEmailValidator());
        }
       
    }
}