﻿using AwsInfrastructureDemo.Validations.CustomRules;
using FluentValidation.Resources;
using FluentValidation.Validators;

namespace AwsInfrastructureDemo.Validations.Localization
{
    public class EnglishLanguage : Language
    {
        public override string Name => CULTURE;

        public const string CULTURE = "en";

        public EnglishLanguage()
        {
            Translate<GreaterThanOrEqualValidator>("Field must be greater than or equal to '{ComparisonValue}'.");
            Translate<GreaterThanValidator>("'Field must be greater than '{ComparisonValue}'.");
            Translate<LengthValidator>("Field length must be between {MinLength} and {MaxLength} characters.");
            Translate<MinimumLengthValidator>("The length of field must be at least {MinLength} characters.");
            Translate<MaximumLengthValidator>("The length of field must be {MaxLength} characters or fewer.");
            Translate<LessThanOrEqualValidator>("Field must be less than or equal to '{ComparisonValue}'.");
            Translate<LessThanValidator>("Field must be less than '{ComparisonValue}'.");
            Translate<NotEmptyValidator>("Field must not be empty");
            Translate<NotEqualValidator>("Field must not be equal to '{ComparisonValue}'.");
            Translate<NotNullValidator>("Field must not be empty");
            Translate<PredicateValidator>("The specified condition was not met for field'.");
            Translate<AsyncPredicateValidator>("The specified condition was not met for field.");
            Translate<RegularExpressionValidator>("Field is not in the correct format.");
            Translate<EqualValidator>("Field confirmation does not match.");
            Translate<ExactLengthValidator>("Field must be {MaxLength} characters in length.");
            Translate<InclusiveBetweenValidator>("Field must be between {From} and {To}.");
            Translate<ExclusiveBetweenValidator>("Field must be between {From} and {To} (exclusive).");
            Translate<CreditCardValidator>("Field is not a valid credit card number.");
            Translate<ScalePrecisionValidator>("Field must not be more than {ExpectedPrecision} digits in total, with allowance for {ExpectedScale} decimals.");
            Translate<EmptyValidator>("Field must be empty");
            Translate<NullValidator>("Field must be empty");
            Translate<EnumValidator>("Field has a range of values which does not include '{PropertyValue}'.");
            Translate<HasFileExtensionValidator>("This field has invalid file extension");
            Translate<IsDigitsValidator>("A value must be a digits");
            Translate<PasswordLengthValidator>("Password length must be from '{MinLength}' to '{MaxLength}' characters.");
        }
    }
}
