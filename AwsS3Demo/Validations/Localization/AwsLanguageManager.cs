﻿using FluentValidation.Resources;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;

namespace AwsInfrastructureDemo.Validations.Localization
{
    public class AwsLanguageManager : ILanguageManager
    {
        private readonly ConcurrentDictionary<string, Language> _languages;
        private readonly Language _fallback = new EnglishLanguage();

        public AwsLanguageManager()
        {
            _languages = new ConcurrentDictionary<string, Language>(new[] {
        new KeyValuePair<string, Language>(EnglishLanguage.CULTURE, _fallback)
      });
        }

        private static Language CreateLanguage(string culture)
        {
            switch (culture)
            {
                case EnglishLanguage.CULTURE:
                    return new EnglishLanguage();
                default:
                    return new EnglishLanguage();
            }
        }

        public bool Enabled { get; set; } = true;
        public CultureInfo Culture { get; set; }

        public string GetString(string key, CultureInfo culture = null)
        {
            string value;

            if (Enabled)
            {
                culture = Culture ?? CultureInfo.CurrentUICulture;
                Language languageToUse = GetCachedLanguage(culture) ?? _fallback;
                value = languageToUse.GetTranslation(key);

                // Selected language is missing a translation for this key - fall back to English translation
                // if we're not using english already.
                if (string.IsNullOrEmpty(value) && languageToUse != _fallback)
                {
                    value = _fallback.GetTranslation(key);
                }
            }
            else
            {
                value = _fallback.GetTranslation(key);
            }

            return value ?? string.Empty;
        }

        public void Clear()
        {
            _languages.Clear();
        }

        private Language GetCachedLanguage(CultureInfo culture)
        {
            // Find matching translations.
            Language languageToUse = _languages.GetOrAdd(culture.Name, CreateLanguage);

            // If we couldn't find translations for this culture, and it's not a neutral culture
            // then try and find translations for the parent culture instead.
            if (languageToUse == null && !culture.IsNeutralCulture)
            {
                languageToUse = _languages.GetOrAdd(culture.Parent.Name, CreateLanguage);
            }

            return languageToUse;
        }

        public void AddTranslation(string language, string key, string message)
        {
            if (string.IsNullOrEmpty(language)) throw new ArgumentNullException(nameof(language));
            if (string.IsNullOrEmpty(key)) throw new ArgumentNullException(nameof(key));
            if (string.IsNullOrEmpty(message)) throw new ArgumentNullException(nameof(message));

            Language languageInstance = _languages.GetOrAdd(language, c => CreateLanguage(c) ?? new GenericLanguage(c));
            languageInstance.Translate(key, message);
        }
    }

    internal class GenericLanguage : Language
    {
        public GenericLanguage(string name)
        {
            Name = name;
        }

        public override string Name { get; }
    }
}
