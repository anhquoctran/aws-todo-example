﻿using AwsInfrastructureDemo.Infrastructure.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace AwsInfrastructureDemo.Models.Todos
{
    public class TodoListViewModel
    {
        [FromQuery(Name = "page")]
        public int? Page { get; set; }

        [FromQuery(Name = "page_size")]
        public int? PageSize { get; set; }

        [FromQuery(Name = "keyword")]
        public string Keyword { get; set; }

        public PaginatedList<TodoDto> Results { get; set; }
    }
}