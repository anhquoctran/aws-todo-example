﻿using Microsoft.AspNetCore.Mvc;

namespace AwsInfrastructureDemo.Models.Users
{
    public class LoginViewModel
    {
        [FromForm(Name = "username")]
        public string Username { get; set; }

        [FromForm(Name = "password")]
        public string Password { get; set; }

        [FromForm(Name = "remember")]
        public bool? RememberMe { get; set; }

        public string CommonErrors { get; set; }
    }
}
