﻿using AwsInfrastructureDemo.Infrastructure.Helpers;
using AwsInfrastructureDemo.Infrastructure.Services.Interfaces;
using AwsInfrastructureDemo.Core.Enums;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

namespace AwsInfrastructureDemo.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AwsAuthorizeAttribute : Attribute, IAuthorizationFilter, IOrderedFilter
    {
        public int Order => int.MinValue + 10;

        /// <summary>
        /// On authorization executing
        /// </summary>
        /// <param name="context">Authorization filter context</param>
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var session = context.HttpContext.GetAwsDemoSession();
            if (session != null)
            {
                if (session.Id.HasValue && session.Id > 0 && !StringUtils.CheckIsEmptyOrWhitespaceString(session.Username))
                {
                    var authService = context.HttpContext.RequestServices.GetRequiredService<IUserService>();
                    var userExists = authService.Authorize(session.Id.Value);
                    if (userExists != OperationResult.Succeed)
                    {
                        context.Result = GetUnauthorizedResult();
                    }

                }
                else
                {
                    //Unauthorized 401 -- redirect to home page
                    context.Result = GetUnauthorizedResult();
                }
            }
            else
            {
                //Unauthorized 401 -- redirect to home page
                context.Result = GetUnauthorizedResult();
            }
        }

        private static IActionResult GetUnauthorizedResult()
        {
            return new RedirectToRouteResult(
                new RouteValueDictionary
                    {
                        { "action", "ShowLogin" },
                        { "controller", "Auth" }
                    });
        }
    }
}
