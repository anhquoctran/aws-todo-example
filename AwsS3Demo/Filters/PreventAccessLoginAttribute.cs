﻿
using AwsInfrastructureDemo.Infrastructure.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using System;

namespace AwsInfrastructureDemo.Filters
{
    public class PreventAccessLoginAttribute : Attribute, IActionFilter, IOrderedFilter
    {
        public int Order => int.MinValue + 20;

        public void OnActionExecuted(ActionExecutedContext context) { }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var session = context.HttpContext.GetAwsDemoSession();
            if (session?.Id != null)
            {
                context.Result = SetRedirect(RedirectTo.Home);
            }
        }

        private static IActionResult SetRedirect(RedirectTo target)
        {
            IActionResult action;
            switch (target)
            {
                case RedirectTo.Login:
                    action = new RedirectToRouteResult
                    (
                        new RouteValueDictionary
                        {
                            { "controller", "Auth" },
                            { "action", "ShowLogin" }
                        }
                    );
                    break;
                default:
                    action = new RedirectToRouteResult
                    (
                        new RouteValueDictionary
                        {
                            { "controller", "Home" },
                            { "action", "Index" }
                        }
                    );
                    break;
            }

            return action;
        }

        private enum RedirectTo
        {
            Home, Login
        }
    }
}
