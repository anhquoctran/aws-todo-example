﻿using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace AwsInfrastructureDemo.Localization
{
    public class JsonStringLocalizer : IStringLocalizer
    {
        private readonly List<JsonLocalization> _localization = new List<JsonLocalization>();
        public JsonStringLocalizer()
        {
            ReadJsonFile();
        }


        public LocalizedString this[string name]
        {
            get
            {
                var value = GetString(name);
                return new LocalizedString(name, value ?? name, resourceNotFound: value == null);
            }
        }

        public LocalizedString this[string name, params object[] arguments]
        {
            get
            {
                var format = GetString(name);
                var value = string.Format(format ?? name, arguments);
                return new LocalizedString(name, value, resourceNotFound: format == null);
            }
        }

        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures)
        {
            var locale = _localization
                .FirstOrDefault(l => l.Locale == "ja-jp");

            return locale?.LocalizedValue
              .Select(l => new LocalizedString(l.Key, l.Value, true));
        }

        public IStringLocalizer WithCulture(CultureInfo culture)
        {
            return new JsonStringLocalizer();
        }

        private string GetString(string name)
        {
            var locale = _localization.FirstOrDefault(l => l.Locale == "ja-jp");
            var value = string.Empty;
            var tryGetValue = locale != null && locale.LocalizedValue.TryGetValue(name, out value);
            return tryGetValue ? value : name;
        }

        private void ReadJsonFile()
        {
            try
            {
                var resourcePath = Path.Combine(Startup.EndpointPath, "Resources");
                Directory.CreateDirectory(resourcePath);
                var ciSupported = Startup.SupportedCultures.Select(ci => ci.Name.ToLower()).ToArray();
                foreach (var ciName in ciSupported)
                {
                    var jsonFilename = $"{ciName}.json";
                    var jsonFilepath = Path.Combine(resourcePath, jsonFilename);
                    if (!File.Exists(jsonFilepath))
                    {
                        File.WriteAllText(jsonFilepath, "{}");
                    }
                    var jsonLocalizationData = File.ReadAllText(jsonFilepath).Trim();
                    var localizedData = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonLocalizationData);
                    if (_localization.All(l => l.Locale != ciName))
                    {
                        _localization.Add(new JsonLocalization
                        {
                            Locale = ciName,
                            LocalizedValue = localizedData
                        });
                    }
                    else
                    {
                        var existing = _localization.FirstOrDefault(l => l.Locale == ciName);
                        if (existing != null) existing.LocalizedValue = localizedData;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
    }
}
