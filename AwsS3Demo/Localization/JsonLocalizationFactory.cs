﻿using Microsoft.Extensions.Localization;
using System;

namespace AwsInfrastructureDemo.Localization
{
    public class JsonLocalizationFactory : IStringLocalizerFactory
    {
        public IStringLocalizer Create(Type resourceSource)
        {
            return new JsonStringLocalizer();
        }

        public IStringLocalizer Create(string baseName, string location)
        {
            return new JsonStringLocalizer();
        }
    }
}
