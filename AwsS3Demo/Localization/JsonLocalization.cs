﻿using System.Collections.Generic;

namespace AwsInfrastructureDemo.Localization
{
    public class JsonLocalization
    {
        public string Locale { get; set; } = "ja-jp";
        public Dictionary<string, string> LocalizedValue = new Dictionary<string, string>();
    }
}
