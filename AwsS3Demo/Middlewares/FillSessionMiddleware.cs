﻿
using AwsInfrastructureDemo.Infrastructure.Constants;
using AwsInfrastructureDemo.Infrastructure.Helpers;
using AwsInfrastructureDemo.Infrastructure.Services.Interfaces;
using AwsInfrastructureDemo.Infrastructure.Sessions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace AwsInfrastructureDemo.Middlewares
{
    public class FillSessionMiddleware
    {
        private readonly RequestDelegate _next;

        public FillSessionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            if (!httpContext.Request.Path.StartsWithSegments(HttpConstants.STATIC_ROUTE_PREFIX))
            {
                var koenkyokaiSession = new AwsDemoSession(httpContext.Session);
                //if (koenkyokaiSession.Id > 0)
                //{
                //    var adminService = httpContext.RequestServices.GetRequiredService<IUserService>();
                //    var (flag, admin) = adminService.GetAdminDetail(koenkyokaiSession.Id.GetValueOrDefault());
                //    if (flag == BLL.Enums.OperationResult.Succeed)
                //    {
                //        koenkyokaiSession.RefreshLoginCache(admin);
                //    }
                //}
                httpContext.RegisterAwsDemoSession(koenkyokaiSession);
            }
            if (!httpContext.Response.HasStarted)
            {
                await _next(httpContext);
            }
        }
    }

    public static class HttpSessionMiddlewareExtension
    {
        public static IApplicationBuilder UseKoenkyokaiSessionMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<FillSessionMiddleware>();
        }
    }
}