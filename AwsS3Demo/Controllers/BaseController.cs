﻿using AwsInfrastructureDemo.Core.Enums;
using AwsInfrastructureDemo.Infrastructure.Helpers;
using AwsInfrastructureDemo.Infrastructure.Sessions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AwsInfrastructureDemo.Controllers
{
    public class BaseController : Controller
    {
        protected IAwsDemoSession AwsDemoSession;
        protected readonly IStringLocalizer Localizer;
        protected readonly ILogger Logger;

        public BaseController(IStringLocalizer localizer, ILogger logger)
        {
            Localizer = localizer;
            Logger = logger;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            Thread.CurrentThread.CurrentCulture = Startup.DefaultCultureInfo;
            Thread.CurrentThread.CurrentUICulture = Startup.DefaultCultureInfo;

            AwsDemoSession = context.HttpContext.GetAwsDemoSession();
            base.OnActionExecuting(context);
        }

        [NonAction]
        public ViewResult GetErrorView(HttpStatus statusCode, params string[] messages)
        {
            var errorCode = (int)statusCode;
            ViewData["ErrorCode"] = errorCode;
            var (title, description) = GetHttpStatusText(statusCode);

            ViewData["ErrorTitle"] = title;

            if (messages.Any())
            {
                ViewData["ErrorDescription"] = messages;
            }
            else
            {
                ViewData["ErrorDescription"] = new[] { description };
            }

            return View("~/Views/Error.cshtml");
        }

        private (string, string) GetHttpStatusText(HttpStatus statusCode)
        {
            string title;
            string description;
            switch (statusCode)
            {
                case HttpStatus.NotFound:
                    Response.StatusCode = (int)HttpStatus.NotFound;
                    description = Localizer["msg.error_not_found"].Value;
                    title = "コンテンツが存在しません";
                    break;
                case HttpStatus.ServerError:
                    Response.StatusCode = (int)HttpStatus.ServerError;
                    description = Localizer["msg.error_server_error"].Value;
                    title = "サーバーシステムエラー";
                    break;
                case HttpStatus.BadRequest:
                    Response.StatusCode = (int)HttpStatus.BadRequest;
                    description = Localizer["msg.error_bad_request"].Value;
                    title = "管理画面";
                    break;
                case HttpStatus.Forbidden:
                    Response.StatusCode = (int)HttpStatus.Forbidden;
                    title = "アクセスが拒否されました";
                    description = Localizer["msg.error_not_found"].Value;
                    break;
                default:
                    Response.StatusCode = (int)HttpStatus.NotFound;
                    description = Localizer["msg.error_not_found"].Value;
                    title = "コンテンツが存在しません";
                    break;
            }
            return (title, description);
        }
    }
}
