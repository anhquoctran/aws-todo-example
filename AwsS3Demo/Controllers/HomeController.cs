﻿using AwsInfrastructureDemo.Core.Enums;
using AwsInfrastructureDemo.Filters;
using AwsInfrastructureDemo.Infrastructure.Constants;
using AwsInfrastructureDemo.Infrastructure.Services.Interfaces;
using AwsInfrastructureDemo.Models.Todos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AwsInfrastructureDemo.Controllers
{
    [Route(""), AwsAuthorize]
    public class HomeController : BaseController
    {
        private readonly ITodoService _todoService;

        public HomeController(
            IStringLocalizer localizer,
            ILogger<HomeController> logger,
            ITodoService todoService
            ) : base(localizer, logger)
        {
            _todoService = todoService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(TodoListViewModel viewModel)
        {
            var data = await _todoService.GetListTodoPagination(viewModel.Keyword, viewModel.Page ?? 1, viewModel.PageSize ?? Pagination.DEFAULT_PAGE_SIZE);
            if (data != null)
            {
                viewModel.Results = data;
                return View(viewModel);
            }

            return GetErrorView(HttpStatus.ServerError);
        }
    }
}