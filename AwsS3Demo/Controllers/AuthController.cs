﻿using AwsInfrastructureDemo.Filters;
using AwsInfrastructureDemo.Infrastructure.Services.Interfaces;
using AwsInfrastructureDemo.Models.Users;
using AwsInfrastructureDemo.Infrastructure.DTOs;
using AwsInfrastructureDemo.Core.Enums;
using AwsInfrastructureDemo.Infrastructure.Helpers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Localization;

namespace AwsInfrastructureDemo.Controllers
{
    [Route("auth")]
    public class AuthController : BaseController
    {
        private readonly IUserService _userService;

        public AuthController(
            IStringLocalizer localizer,
            ILogger<AuthController> logger,
            IUserService userService) : base(localizer, logger)
        {
            _userService = userService;
        }

        [HttpGet("login"), PreventAccessLogin]
        public IActionResult ShowLogin()
        {
            if (TempData.ContainsKey("LOGIN_VALIDATE_ERRORS"))
            {
                var loginValidateMessages = TempData["LOGIN_VALIDATE_ERRORS"]?.ToString();
                if (!StringUtils.CheckIsEmptyOrWhitespaceString(loginValidateMessages))
                {
                    ModelState.SetModelStateError(JsonConvert.DeserializeObject<Dictionary<string, string[]>>(loginValidateMessages));
                }
            }

            var viewModel = new LoginViewModel();
            
            if (TempData.ContainsKey("LOGIN_LAST_FIELD_DATA"))
            {
                var lastLoginInfo = TempData["LOGIN_LAST_FIELD_DATA"]?.ToString();
                if (!StringUtils.CheckIsEmptyOrWhitespaceString(lastLoginInfo))
                {
                    viewModel = JsonConvert.DeserializeObject<LoginViewModel>(lastLoginInfo);
                }
            }
            return View(viewModel);
        }

        [HttpPost("login"), PreventAccessLogin]
        public async Task<IActionResult> Login(LoginViewModel viewModel)
        {
            TempData["LOGIN_LAST_FIELD_DATA"] = JsonConvert.SerializeObject(viewModel);
            if (ModelState.IsValid)
            {
                var isLoginOk = await _userService.Login(new LoginQueryDto
                {
                    Username = viewModel.Username,
                    Password = viewModel.Password
                });

                if (isLoginOk.Result == OperationResult.ExceptionFailed)
                {
                    return GetErrorView(HttpStatus.ServerError);
                }

                if (isLoginOk.Result == OperationResult.Failed)
                {
                    TempData["LOGIN_VALIDATE_ERRORS"] = JsonConvert.SerializeObject(new Dictionary<string, string[]> 
                    {
                        { "CommonErrors", new string[] { Localizer[isLoginOk.Message] } }
                    });
                    return RedirectToAction("ShowLogin");
                }

                AwsDemoSession.Id = isLoginOk.UserId;
                AwsDemoSession.DisplayName = isLoginOk.Name;
                AwsDemoSession.Username = isLoginOk.Username;
                AwsDemoSession.LoginAt = DateTime.UtcNow;
                AwsDemoSession.SaveChanges();

                TempData.Remove("LOGIN_LAST_FIELD_DATA");
                return RedirectToAction("Index", "Home");
            }

            var messages = ModelState.GetMessagesWithKey();
            TempData["LOGIN_VALIDATE_ERRORS"] = JsonConvert.SerializeObject(messages);
            return RedirectToAction("ShowLogin");
        }
    }
}
