﻿using Amazon.S3;
using AwsInfrastructureDemo.Core;
using AwsInfrastructureDemo.Core.UnitOfWorks;
using AwsInfrastructureDemo.Infrastructure.Mappings;
using AwsInfrastructureDemo.Infrastructure.Services;
using AwsInfrastructureDemo.Localization;
using AwsInfrastructureDemo.Middlewares;
using AwsInfrastructureDemo.Validations.Localization;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;

namespace AwsInfrastructureDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public static readonly CultureInfo DefaultCultureInfo = new CultureInfo("en-US");

        public static readonly RequestCulture DefaultRequestCulture = new RequestCulture(DefaultCultureInfo);

        public static readonly HashSet<CultureInfo> SupportedCultures = new HashSet<CultureInfo>
        {
            new CultureInfo("vi-VN"),
            DefaultCultureInfo
        };

        public static readonly string EndpointPath = GetEnpointPath();

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            // so Tempdata is functional when tracking is disabled.
            services.Configure<CookieTempDataProviderOptions>(options =>
            {
                options.Cookie.IsEssential = true;
            });

            services.AddLocalization(options =>
            {
                options.ResourcesPath = "Resources";
            });

            services.AddLogging();

            services.AddControllersWithViews()
             .AddRazorRuntimeCompilation();

            services.AddMvc()

                .SetCompatibilityVersion(CompatibilityVersion.Latest)
                .AddFluentValidation(fv =>
                {
                    fv.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly());
                    ValidatorOptions.LanguageManager = new AwsLanguageManager();
                })
                .AddSessionStateTempDataProvider()
                .AddMvcOptions(options =>
                {
                    options.MaxModelValidationErrors = 50;
                    //options.ModelBindingMessageProvider.SetAttemptedValueIsInvalidAccessor((x, y) => "数字以外の文字が入力されています");
                });

            services.AddDataProtection();

            services.AddRouting(options =>
            {
                options.LowercaseQueryStrings = true;
                options.LowercaseQueryStrings = true;
            });

            services.AddDbContext<AwsDemoDbContext>(options =>
            {
                var connectionString = Configuration["ConnectionStrings:Default"];
                options.UseSqlServer(connectionString, opt =>
                {
                    opt.CommandTimeout(1800);
                });
            });

            services.AddDistributedMemoryCache();

            services.AddAwsMapping();

            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromDays(30);
                options.Cookie.HttpOnly = true;
                // Make the session cookie essential
                options.Cookie.IsEssential = true;
                options.Cookie.Name = "AWS_DEMO_SID";
            });

            services.AddDefaultAWSOptions(Configuration.GetAWSOptions());
            services.AddAWSService<IAmazonS3>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.RegisterAllServices();

            ConfigureGlobalization(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            CultureInfo.DefaultThreadCurrentCulture = DefaultCultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = DefaultCultureInfo;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseSession();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseCookiePolicy();

            app.UseKoenkyokaiSessionMiddleware();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private static void ConfigureGlobalization(IServiceCollection services)
        {
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var listSupportedCultures = SupportedCultures.ToList();
                options.DefaultRequestCulture = DefaultRequestCulture;
                options.SupportedCultures = listSupportedCultures;
                options.SupportedUICultures = listSupportedCultures;
            });

            services.AddSingleton<IStringLocalizerFactory, JsonLocalizationFactory>();
            services.AddSingleton<IStringLocalizer, JsonStringLocalizer>();
            services.AddLocalization(options => options.ResourcesPath = "Resources");
        }

        /// <summary>
        /// Get application runtime endpoint absolute path
        /// </summary>
        /// <returns></returns>
        private static string GetEnpointPath()
        {
            var asmPath = Assembly.GetExecutingAssembly().CodeBase;

            var executePath = Path.GetDirectoryName(asmPath);
            var result = executePath.Replace("file:\\", "").Replace("file:", "");
            return result;
        }
    }
}