﻿namespace AwsInfrastructureDemo.Core.Enums
{
    public enum OperationResult
    {
        Succeed,
        Failed,
        ExceptionFailed,
        NotFound,
        Forbidden
    }
}