﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AwsInfrastructureDemo.Core.Enums
{
    public enum HttpStatus
    {
        NotFound = 404,
        ServerError = 500,
        BadRequest = 400,
        Forbidden = 403,
        Unauthorized = 401
    }
}
