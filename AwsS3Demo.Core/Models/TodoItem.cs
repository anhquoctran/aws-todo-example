﻿using AwsInfrastructureDemo.Core.Abstractions;
using System;

namespace AwsInfrastructureDemo.Core.Models
{
    public class TodoItem : IIdentificable<int>, ITimestamp
    {
        public int Id { get; set; }
        public int TodoId { get; set; }
        public bool IsDone { get; set; }
        public string TodoContent { get; set; }
        public string TodoImageUrl { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public virtual Todo Todo { get; set; }
    }
}