﻿using AwsInfrastructureDemo.Core.Abstractions;
using System;
using System.Collections.Generic;

namespace AwsInfrastructureDemo.Core.Models
{
    public class Todo : IIdentificable<int>, ITimestamp
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public virtual ICollection<TodoItem> TodoItems { get; set; }
    }
}