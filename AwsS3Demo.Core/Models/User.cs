﻿using AwsInfrastructureDemo.Core.Abstractions;
using System;

namespace AwsInfrastructureDemo.Core.Models
{
    public class User : IIdentificable<int>, ITimestamp
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}