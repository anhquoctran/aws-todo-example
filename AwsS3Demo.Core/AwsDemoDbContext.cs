﻿using AwsInfrastructureDemo.Core.Abstractions;
using AwsInfrastructureDemo.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AwsInfrastructureDemo.Core
{
    public class AwsDemoDbContext : DbContext
    {
        public AwsDemoDbContext() { }

        public AwsDemoDbContext(DbContextOptions<AwsDemoDbContext> contextOptions)
            : base(contextOptions) { }

        public DbSet<User> Users { get; set; }

        public DbSet<Todo> Todos { get; set; }

        public DbSet<TodoItem> TodoItems { get; set; }

        private void InternalSaveChanges()
        {
            var entries = ChangeTracker.Entries()
               .Where(e => (e.Entity is ITimestamp) && (e.State == EntityState.Added || e.State == EntityState.Modified));

            foreach (var entityEntry in entries)
            {
                if (entityEntry.Entity is ITimestamp entity)
                {
                    if (entityEntry.State == EntityState.Added)
                    {
                        if (entity.CreatedAt == DateTime.MinValue)
                        {
                            entity.CreatedAt = DateTime.UtcNow;
                        }
                    }

                    if (entityEntry.State == EntityState.Modified)
                    {
                        if (entity.UpdatedAt == DateTime.MinValue)
                        {
                            entity.UpdatedAt = DateTime.UtcNow;
                        }
                    }
                }
            }
        }

        public override int SaveChanges()
        {
            InternalSaveChanges();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            InternalSaveChanges();
            return base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<User>(entity =>
            {
                entity.ToTable("User");

                entity.HasKey(x => x.Id);

                entity.HasIndex(x => x.Username)
                    .IsUnique();

                entity.Property(x => x.Id)
                    .ValueGeneratedOnAdd();

                entity.Property(x => x.Username)
                    .IsRequired()
                    .HasMaxLength(64);

                entity.Property(x => x.Name)
                    .HasMaxLength(100);

                entity.Property(x => x.Password)
                    .IsRequired()
                    .HasMaxLength(128);
            });

            builder.Entity<Todo>(entity =>
            {
                entity.ToTable("Todo");

                entity.HasKey(x => x.Id);

                entity.HasIndex(x => x.Name)
                    .IsUnique();

                entity.Property(x => x.Id)
                    .ValueGeneratedOnAdd();

                entity.Property(x => x.Name)
                    .IsRequired()
                    .HasMaxLength(120);

                entity.HasMany(x => x.TodoItems)
                    .WithOne(x => x.Todo)
                    .HasForeignKey(x => x.TodoId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<TodoItem>(entity =>
            {
                entity.ToTable("TodoItem");

                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id)
                    .ValueGeneratedOnAdd();

                entity.Property(x => x.TodoContent)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(x => x.TodoImageUrl)
                    .HasMaxLength(255);

                entity.Property(x => x.IsDone)
                    .IsRequired()
                    .HasDefaultValue(false);

                entity.HasOne(x => x.Todo)
                    .WithMany(x => x.TodoItems)
                    .HasForeignKey(x => x.TodoId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            base.OnModelCreating(builder);
        }
    }
}