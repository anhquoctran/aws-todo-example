﻿using AwsInfrastructureDemo.Core.Models;
using AwsInfrastructureDemo.Core.Repositories;
using Microsoft.EntityFrameworkCore.Storage;
using System.Threading;
using System.Threading.Tasks;

namespace AwsInfrastructureDemo.Core.UnitOfWorks
{
    public interface IUnitOfWork
    {
        #region Repositories

        IRepository<User> UserRepository { get; }

        IRepository<Todo> TodoRepository { get; }

        IRepository<TodoItem> TodoItemRepository { get; }

        #endregion Repositories

        #region Methods

        IDbContextTransaction BeginTransaction();

        Task<IDbContextTransaction> BeginTransactionAsync(CancellationToken cancellationToken = default(CancellationToken));

        Task SaveAsync(CancellationToken cancellationToken = default(CancellationToken));

        void Save();

        #endregion Methods
    }
}