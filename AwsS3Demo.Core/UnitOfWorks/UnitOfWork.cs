﻿using AwsInfrastructureDemo.Core.Models;
using AwsInfrastructureDemo.Core.Repositories;
using Microsoft.EntityFrameworkCore.Storage;
using System.Threading;
using System.Threading.Tasks;

namespace AwsInfrastructureDemo.Core.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AwsDemoDbContext _dbContext;

        public IRepository<User> UserRepository { get; private set; }
        public IRepository<Todo> TodoRepository { get; private set; }
        public IRepository<TodoItem> TodoItemRepository { get; private set; }

        public UnitOfWork(AwsDemoDbContext dbContext)
        {
            _dbContext = dbContext;
            InitializeRepos();
        }

        private void InitializeRepos()
        {
            UserRepository = new Repository<User>(_dbContext);
            TodoRepository = new Repository<Todo>(_dbContext);
            TodoItemRepository = new Repository<TodoItem>(_dbContext);
        }

        public IDbContextTransaction BeginTransaction()
        {
            return _dbContext.Database.BeginTransaction();
        }

        public Task<IDbContextTransaction> BeginTransactionAsync(CancellationToken cancellationToken = default)
        {
            return _dbContext.Database.BeginTransactionAsync(cancellationToken);
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        public async Task SaveAsync(CancellationToken cancellationToken = default)
        {
            await _dbContext.SaveChangesAsync(cancellationToken);
        }
    }
}