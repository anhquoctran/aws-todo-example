﻿namespace AwsInfrastructureDemo.Core.Abstractions
{
    public interface IIdentificable<TPrimaryKey> : IEntity
    {
        TPrimaryKey Id { get; set; }
    }
}