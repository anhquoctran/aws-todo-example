﻿using System;

namespace AwsInfrastructureDemo.Core.Abstractions
{
    public interface ITimestamp
    {
        DateTime CreatedAt { get; set; }

        DateTime UpdatedAt { get; set; }
    }
}