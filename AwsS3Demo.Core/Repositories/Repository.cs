﻿using AwsInfrastructureDemo.Core.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace AwsInfrastructureDemo.Core.Repositories
{
    public class Repository<T> : IRepository<T> where T : class, IEntity
    {
        private readonly AwsDemoDbContext _context;
        private readonly DbSet<T> _dbSet;

        public Repository(AwsDemoDbContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }

        public void Delete(T registry)
        {
            _dbSet.Remove(registry);
            _context.SaveChanges();
        }

        public int DeleteBy(Expression<Func<T, bool>> predicate)
        {
            if (predicate == null) return -1;
            return _dbSet.AsQueryable().Where(predicate)
                .Delete();
        }

        public Task DeleteAsync(T entity, CancellationToken cts = default(CancellationToken))
        {
            _dbSet.Remove(entity);
            return _context.SaveChangesAsync(cts);
        }

        public async Task<int> DeleteByAsync(Expression<Func<T, bool>> predicate, CancellationToken cts = default(CancellationToken))
        {
            if (predicate == null) return -1;
            return await _dbSet.AsQueryable().Where(predicate)
                .DeleteAsync(cts);
        }

        public Task<int> DeleteAsync(IEnumerable<T> entites, CancellationToken cts = default(CancellationToken))
        {
            return _dbSet.AsQueryable().Where(x => entites.Contains(x))
                .DeleteAsync(cts);
        }

        /// <summary>
        /// Delete multiple items with list of ID
        /// </summary>
        /// <param name="listId"></param>
        public void Delete(IEnumerable<T> listId)
        {
            _dbSet.AsQueryable().Where(x => listId.Contains(x))
                .Delete();
        }

        public ICollection<T> GetAll(Expression<Func<T, bool>> predicate = null)
        {
            if (predicate != null)
            {
                return _dbSet
                    .AsQueryable()
                    .Where(predicate)
                    .ToList();
            }
            return _dbSet.AsQueryable().ToList();
        }

        public async Task<ICollection<T>> GetAllAsync(Expression<Func<T, bool>> predicate = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (predicate != null)
            {
                return await _dbSet
                    .AsQueryable()
                    .Where(predicate)
                    .ToListAsync(cancellationToken);
            }
            return await _dbSet.AsQueryable().ToListAsync(cancellationToken);
        }

        public T Add(T registry)
        {
            _dbSet.Add(registry);
            _context.SaveChanges();
            return registry;
        }

        /// <summary>
        /// Check entity exists by property
        /// </summary>
        /// <param name="predicate">Predicate condition to check</param>
        /// <returns></returns>
        public bool ExistsBy(Expression<Func<T, bool>> predicate)
        {
            return _dbSet.Any(predicate);
        }

        public Task<bool> ExistsByAsync(Expression<Func<T, bool>> predicate, CancellationToken cts = default(CancellationToken))
        {
            return _dbSet.AnyAsync(predicate, cts);
        }

        /// <summary>
        /// Find item by predicate condition. If condition is null, return the first element of default
        /// </summary>
        /// <param name="predicate">Condition</param>
        /// <param name="noTracking"></param>
        /// <returns></returns>
        public T Find(Expression<Func<T, bool>> predicate, bool noTracking = false)
        {
            if (noTracking)
            {
                return predicate != null ? _dbSet.AsNoTracking().FirstOrDefault(predicate) : _dbSet.AsNoTracking().FirstOrDefault();
            }

            return predicate != null ? _dbSet.FirstOrDefault(predicate) : _dbSet.FirstOrDefault();
        }

        public async Task<T> FindAsync(
            Expression<Func<T, bool>> predicate,
            bool noTracking = false,
            CancellationToken token = default(CancellationToken)
            )
        {
            if (noTracking)
            {
                return await (predicate != null ? _dbSet
                    .AsNoTracking()
                    .FirstOrDefaultAsync(predicate, token) : _dbSet.AsNoTracking().FirstOrDefaultAsync(token));
            }

            return await (predicate != null ? _dbSet.FirstOrDefaultAsync(predicate, token) : _dbSet.FirstOrDefaultAsync(token));
        }

        /// <summary>
        /// Count with predicate condition
        /// </summary>
        /// <param name="predicate">Condition</param>
        /// <returns></returns>
        public int Count(Expression<Func<T, bool>> predicate = null)
        {
            return predicate != null ? _dbSet.Count(predicate) : _dbSet.Count();
        }

        public async Task<int> CountAsync(
            Expression<Func<T, bool>> predicate = null,
            CancellationToken token = default(CancellationToken)
            )
        {
            return predicate != null ? await _dbSet.CountAsync(predicate, token) : await _dbSet.CountAsync(token);
        }

        public int MaxBy(Expression<Func<T, int>> predicate)
        {
            return _dbSet.Max(predicate);
        }

        public Task<int> MaxByAsync(Expression<Func<T, int>> predicate, CancellationToken token = default(CancellationToken))
        {
            return _dbSet.MaxAsync(predicate, token);
        }

        public T Update(T entity)
        {
            var existing = Find(x => x == entity);
            _context.Entry(existing).CurrentValues.SetValues(entity);

            return entity;
        }

        public int UpdateBy(Expression<Func<T, bool>> condition, Expression<Func<T, T>> updateFactory)
        {
            return _dbSet.AsQueryable().Where(condition).Update(updateFactory);
        }

        public Task<int> UpdateByAsync(
            Expression<Func<T, bool>> condition,
            Expression<Func<T, T>> updateFactory,
            CancellationToken cts = default(CancellationToken)
            )
        {
            return _dbSet.AsQueryable().Where(condition)
                .UpdateAsync(updateFactory, cts);
        }

        public IQueryable<T> AsQueryable(Expression<Func<T, bool>> predicate)
        {
            if (predicate != null)
            {
                return _dbSet.AsQueryable()
                    .Where(predicate);
            }

            return _dbSet.AsQueryable();
        }

        public void UpdateRange(IEnumerable<T> entites)
        {
            _dbSet.UpdateRange(entites);
        }
    }
}