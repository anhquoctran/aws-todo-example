﻿using AwsInfrastructureDemo.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace AwsInfrastructureDemo.Core.Repositories
{
    public interface IRepository<T> where T : class, IEntity
    {
        T Add(T entity);

        T Update(T entity);

        int UpdateBy(Expression<Func<T, bool>> condition, Expression<Func<T, T>> updateFactory);

        Task<int> UpdateByAsync(
            Expression<Func<T, bool>> condition,
            Expression<Func<T, T>> updateFactory,
            CancellationToken cts = default(CancellationToken)
            );

        void UpdateRange(IEnumerable<T> entites);

        void Delete(T entity);

        int DeleteBy(Expression<Func<T, bool>> predicate);

        Task DeleteAsync(T entity, CancellationToken cts = default(CancellationToken));

        Task<int> DeleteByAsync(Expression<Func<T, bool>> predicate, CancellationToken cts = default(CancellationToken));

        Task<int> DeleteAsync(IEnumerable<T> entites, CancellationToken cts = default(CancellationToken));

        void Delete(IEnumerable<T> entites);

        ICollection<T> GetAll(Expression<Func<T, bool>> predicate = null);

        Task<ICollection<T>> GetAllAsync(Expression<Func<T, bool>> predicate = null, CancellationToken cancellationToken = default(CancellationToken));

        IQueryable<T> AsQueryable(Expression<Func<T, bool>> predicate = null);

        bool ExistsBy(Expression<Func<T, bool>> predicate);

        Task<bool> ExistsByAsync(Expression<Func<T, bool>> predicate,
            CancellationToken cts = default(CancellationToken));

        T Find(Expression<Func<T, bool>> predicate, bool noTracking = false);

        Task<T> FindAsync(Expression<Func<T, bool>> predicate, bool noTracking = false, CancellationToken token = default(CancellationToken));

        int Count(Expression<Func<T, bool>> predicate = null);

        Task<int> CountAsync(Expression<Func<T, bool>> predicate = null, CancellationToken token = default(CancellationToken));

        int MaxBy(Expression<Func<T, int>> predicate);

        Task<int> MaxByAsync(Expression<Func<T, int>> predicate, CancellationToken token = default(CancellationToken));
    }
}